# Ressources

<br>

## General

https://www.reactnative.guide/index.html

## Architecture

[Best Practice to make project structure](https://stackoverflow.com/questions/60647270/best-practice-to-make-project-structure-react-native)

<br>

## Redux

[How to integrate Redux into your application with React Native and Expo](https://www.freecodecamp.org/news/how-to-integrate-redux-into-your-application-with-react-native-and-expo-ec37c9ca6033/)

[Using Redux Toolkit in React Native: Getting started and usage guide](https://hybridheroes.de/blog/2021-01-08-redux-toolkit-react-native/)

[How to use Redux-Persist with Redux-Toolkit](https://www.google.com/search?channel=fs&client=ubuntu&q=redux-persist+redux-toolkit)

[How To Use Redux Persist in React Native with Asyncstorage](https://blog.jscrambler.com/how-to-use-redux-persist-in-react-native-with-asyncstorage/)

[Simple Redux Persist configuration in React Native / Expo environment](https://medium.com/survival-development/simple-redux-persist-configuration-in-react-native-expo-environment-5cae7c4a22)
<br>

[https://github.com/neverdull-agency/expo-unlimited-secure-store](https://github.com/neverdull-agency/expo-unlimited-secure-store)
not used anymore => [https://github.com/Cretezy/redux-persist-expo-securestore](https://github.com/Cretezy/redux-persist-expo-securestore)

## Navigation

[React Navigation v5 + React Native Paper = ❤️](https://reactnavigation.org/blog/2020/01/29/using-react-navigation-5-with-react-native-paper/)

[createMaterialBottomTabNavigator](https://reactnavigation.org/docs/material-bottom-tab-navigator/)

[Fade-in/FadeOut bottom app](https://www.reddit.com/r/reactnative/comments/imlesa/react_navigation_when_using_the_bottom_tab/)

<br>

## Theming

[React Native paper](https://callstack.github.io/react-native-paper/theming.html)

<br>

## Jest

[React-Redux testing](https://medium.com/@szpytfire/react-redux-testing-mocking-useselector-and-usedispatch-e004c3f2b2e0)
[Mock only one function](https://www.chakshunyu.com/blog/how-to-mock-only-one-function-from-a-module-in-jest/)
[enzyme](https://medium.com/@hdsenevi/unit-testing-in-react-native-with-jest-and-enzyme-40cd7dabb6f1)

## Other

[ShortcutVsCode](https://dev.to/simonpaix/10-useful-vs-code-shortcuts-you-should-know-42m)

---

## [Retour sommaire](../README.md)
