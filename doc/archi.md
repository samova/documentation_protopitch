# Architecture Projet

L'architecture a été pensée en fonction de cet article: [Best Practice to make project structure](https://stackoverflow.com/questions/60647270/best-practice-to-make-project-structure-react-native).

Les noms de l'architecture sont, en générale, assez explicite:

```javascript
.
├── assets
│ 	// Répértoire de la documentation
├── doc
│   └── ressources
│       ├── presentation
│       └── utilisation
│ 	
│ 	// Sources de l'application
├── src
│   │ 	// Appels vers la backend
│   │ 	// Ce sera les sources à modifier si on veut utiliser un autre algorithme
│   ├── api
│   │ 	// Assets (non utiliseés)
│   ├── assets
│   │ 	// Composants (fenêtre latérale comprise)
│   ├── components
│   │ 	// Screens (Mesure & Historique)
│   ├── containers
│   │ 	// Données (Conversion mois en => fr)
│   ├── data
│   │ 	// Navigation (barre d'outil du bas & fenêtre latérale)
│   ├── navigation
│   │ 	// Store redux
│   ├── redux
│   │   └── store
│   │ 	// Styles de l'application 
│   ├── style
│   │ 	// Fonction utilitaires (génération de nom pour les fichiers, ect.)
│   └── utilities
│ 	
│ 	// Tests de l'application
│ 	// Note: architecture similaire au dossier src
└── __tests__ 
    ├── components
    ├── container
    ├── redux
    │ 	// Contients des fichiers executées avant chaque test
    ├── setupFiles
    │ 	// Contients les snapshot pour les test de snpashots
    ├── __snapshots__
    │   └── container
    └── utilities

```



---

### [Retour sommaire](../README.md)

