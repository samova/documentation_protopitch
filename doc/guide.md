# Guide d'utilisation ORL

## Généralités 

La présente application est un **MVP** (pour **M**inimal **V**iable **P**roduct) permettant l'utilisation des travaux de V. Roger sur l'intelligibilité, issues du projets SAMI, en hôpital. 

<br>

## Gestions des patients

![menuPatient](./ressources/guide/menuPatient.png)



Le **menu de gestion des patients** permets de séléctionner ce que l'on appellera le **patient courant**. On pourra alors stocker les mesures associés à ce patient et consulter son historique. Il est accessible via un swipe de l'écran, de la gauche vers la droite, ou l'appuie sur la flèche en haut à gauche.

Plusieurs options sont disponibles: 

* *Sélectionner Patient* permets de séléctionner le **patient courant**.

* *Modifier Patient* permets de modifier les informations personnelles du **patient courant**.

* *Ajouter Patient* permets d'ajouter un patient dans la base de donnée de l'application.

  ![menuPatient](./ressources/guide/ajout.gif)

* *Supprimer Patient*  permets de supprimer un (ou plusieurs) patients.

<br>

## Prendre une mesure

![prise de mesure](./ressources/guide/recording.gif)

L'onglet de prise de mesure est disponible sur la barre en bas de l'écran, sur le premier onglet.

Prendre une mesure se déroule de la manière suivante:

 	1.	Cliquer sur le micro. L'enregistrement va débuter et un texte va s'afficher.
 	2.	Une fois que le patient a fini de lire le texte, cliquer sur le bouton *stop*.
 	3.	Les traitements vont être effectués sur le serveur. On a alors deux possibilités:
      	1.	Si un **patient courant** est sélectionné, le score est sauvegardé pour ce patient courant.
      	2.	Si aucun patient courant n'est sélectionné, le score n'est pas sauvegardé.



<br>

## Consulter l'historique des mesures



![mesures](./ressources/guide/mesure.gif)

Lorsqu'un **patient courant** est selectionné, il est possible de consulter l'historiques de ses mesures. L'historique de mesure se trouve sur le second onglet de la barre en bas de l'écran.

Il est possible de **trier les mesures** par date et par score, ascendant ou descendant, en touchant les en-têtes de colonnes correspondantes. 

On peut aussi **filtrer les mesures**, par score ou date, si le practicien désire voire des plages précises de l'historique.









---

### [Retour sommaire](../README.md)

