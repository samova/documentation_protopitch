# Installation

## Pré-requis (expo/react-native)

Installer avant tout node (voir [les instructions à ce lien](https://docs.expo.dev/get-started/installation/)).

Ensuite, installer react-native / expo (suivre les instructions [à ce lien](https://docs.expo.dev/get-started/installation/)).



## Installation du projet

Toutes les instructions peuvent être exécutées à la suite

##### Cloner le projet

*https:* `git clone https://github.com/GautierArcin/appliAudioCap.git `

**ou** 

*ssh*: `git clone git@github.com:GautierArcin/appliAudioCap.git`



##### Installer le projet

`cd appliAudioCap`

`yarn install`



##### Lancer le projet

`expo start`



---

### [Retour sommaire](../README.md)

