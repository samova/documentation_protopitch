# Test

## Généralités

Le projet utilise deux librairies différentes pour effectuer des test:

 - [Jest](https://jestjs.io/fr/)

 - [Enzyme](https://enzymejs.github.io/enzyme/)

   

![exemple de test](./ressources/test/1.png)



Deux types des test ont été réalisées: Snapshot et unitaire. Il était prévu de faire des test d'intégration et E2E, mais enzyme semble avoir des problèmes avec une autre librairie utilisée pour les contrôles tactiles (cf. [cette issue github](https://github.com/software-mansion/react-native-gesture-handler/issues/1305), toujours sans réponse à l'heure de rédaction de la présente documentation).



**Note:** Un préset spécial de jest est utilisé. On test sur deux environnement différents en même temps: 

- Un simulant android
- Un simulant IOS



## Execution des tests

`yarn test`

On pourra aussi taper `yarn test --watch` (plus pour faire du tdd).



## Ajouts de test



Ajouter des fichiers finissant par `.spec.tsx` ou `.spec.js` dans le dossier `___tests___`



---

### [Retour sommaire](../README.md)

