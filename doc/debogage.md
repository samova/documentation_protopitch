# Débogage

## Généralités

Le projet inclut l'utilisation de [React Native Debugger](https://github.com/jhen0409/react-native-debugger).

![Exemple react native debugger](./ressources/debogage/1.png)



Il permet de suivre l'état de l'application (du store redux complet) afin de mieux débugger, si nécessaire.

Il permet aussi de faire du profilage, permettant de voir quels sont les composants les plus susceptibles d'être optimisés.



## Utilisation

1. Installer `React Native Debugger`.

2. Lancer `React-native debugger`.

3. Permettre le débogage à distance 

   1. Appuyer sur `m` dans la fenêtre où la commande `expo start` a été executée.

   2. Appuyer sur `remote debugging` sur l'ipad (ou le dispositif de test).

      

***Note:*** *Par défaut, react native debugger se lance sur le mauvais port. Il faut le re-lancer sur le port 19000 ou 19001* 



---

### [Retour sommaire](../README.md)

