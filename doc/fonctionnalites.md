# Fonctionnalités

## Généralités

Le but de l'application est de fournir un véritable outil aux praticiens orl, leur permettant d'avoir un indicateur supplémentaire (l'intelligibilité) pour suivre les patients.

L'application a été développée en **react native** (dans un worfklow **expo** et **typescript**) avec comme cible un ipad mini. 



## Enregistrements

![test](./ressources/presentation/enregistrement.gif)

L'application peut enregistrer la parole des différents patients. Un score d'intelligiblitée, entre 0 et 10, sera renvoyé. Ce score est issue des travaux du doctorant Vincent Roger.

Le score sera automatiquement sauvegardé si un patient est sélectionnée. L'enregistrement peut, si le practicien le désire, être stocké sur un serveur externe.



## Gestion des Patients

![test](./ressources/presentation/ajoutPatient.gif)

Le practicien peut sélectionner, enlever, ou rajouter des patients.

Un patient est constitué par son nom complet, son IP (pour Identifiant Patient) et sa date de naissance.

Toutes les données sont stockées dans un stockage asynchrone crypté via AES 256 [via une librairie communautaire](https://www.npmjs.com/package/@neverdull-agency/expo-unlimited-secure-store).



## Historique

![test](./ressources/presentation/historique.gif)

Le practicien aura a sa disposition un second onglet. Il lui permettra de visualiser l'historique des mesures du patient actuellement sélectionné. Il pourra trier les mesures par ordre chronologique ou par score.

Le practicien pourra aussi choisir d'appliquer des filtres sur les données des patients, pour afficher uniquement les données qui l'intéresse.

Finalement, le practicien pourra supprimer une mesure par un appui long.



---

### [Retour sommaire](../README.md)

