# Ajout d'un algo



## Généralités

Cette section couvre comment on peut utiliser le fonctionnement nominatif de l'application pour utiliser d'autres algorithmes (et non celui uniquement de V. Roger). Cette section (et cette option) est pensée pour les travaux des doctorants de l'équipe SAMOVA.



## 1. Ajouter l'algorithme et la route sur la backend.

### a°) Explications générales sur la backend 

Avant d'ajouter l'algorithme, à la backend, il convient de faire un explication succincte du mode de fonctionnement de l'API REST en backend.

Le serveur web est dockerisé dans un conteneur docker installant, par défaut, les dépendances de l'algorithme de V. Roger.

L'API REST dévoile **trois** endpoints: 

 - `/` : renvoie juste `server is up`, permets à l'application de vérifier si le server est en ligne ou non.
 - `/upload/`: permets à l'application d'upload un fichier audio. Les fichiers audio sont automatiquement répartis par patients et par mois.
 - `/intel/getScore/<filename>`: permets à l'application d'executer un algorithme et récupérer un score sur un fichier audio. Dans ce cadre-là, on exécute l'algorithme d'intelligibilité  de V. Roger. 

Pour rajouter un algorithme, il faudra tout d'abord rajouter les dépendances de l'algorithme, puis rajouter un endpoint qui se substituera au 3ièmes endpoint listé précédemment. 



### b°) Rajouter les dépendances

Par défaut, l'environnement python installe un via un fichier `.yaml` les dépendances de l'algorithme de Vincent.

`RUN conda env create -f pase+_environment.yml` pour la création de l'environnement puis  `SHELL ["conda", "run", "-n", "pase+", "/bin/bash", "-c"] ` pour son activation.

D'autres dépendances sont installées ensuite:

```dockerfile
RUN pip install flask
RUN pip install gevent
```

On pourra, au choix: 

- Rajouter les dépendances à l'environnement python (via `RUN pip` ou en combinant les `*.yml`)
- On pourra remplacer l'environnement conda (si on ne souhaite pas exécuter l'algo sur l'intelligibilité,  on n'aura pas besoin des dépendances de cet environnement).



### c°) Rajouter un algorithme et un endpoint

Sur [le repo github de la backend](https://github.com/GautierArcin/dockerAppli/tree/master/docker_flask_api_5mn), il faudra modifier le fichier `server.py` ( [dans scripts/server.py](https://github.com/GautierArcin/dockerAppli/blob/master/docker_flask_api_5mn/scripts/server.py) ).

Il faudra rajouter un 3iéme endpoint qui se subsistera à `/intel/getScore/<filename>`. Il faudra qu'il prenne la route de `/[nomAlgorithme]/getScore/<filename>` (avec `nom algorithme` le nom interne de votre algorithme).

On pourra alors faire tous le traitement dans la route susmentionnée ( `/intel/getScore/<filename>` ),  on fera deux traitements via `ffmpeg` (un pour amplifier l'audio, un pour changer la sample rate pour l'algorithme de V. Roger) et on exécutera l'algorithme de V. Roger en python. 

La fonction devra renvoyer un score, sous forme de string, avec une réponse 200. 

A titre exemple, voici une route random (nommée `rand`):

```python
@app.route('/getScore/rand/<filename>',methods = ['GET'])
def get_rand_score(filename):
	return str(random.randint(1,1000)),200
```



## 2. Ajouter l'algorithme sur le front

Il faudra modifier le fichier `/src/data/Mesures.tsx`. 

Il faudra rajouter un item dans l'objet `routeMeasures`, sous la forme `routeAPI: "Nom affiché dans le menu"`. 

L'application avec une route sera comme ceci: 

![Image 1 route](./ressources/route/1.png)



Si on reprend notre exemple sur la `/getScore/rand/<filename>`:

```typescript
const routeMeasures = {
  intel: "Mesure d'intellegibilité",
  rand: 'Mesure aléatoire',
};
```



Une fois la ligne sur `rand` ajouté, l'application devrait ressembler à ceci: 

![Image 2 route](./ressources/route/2.png)



---

### [Retour sommaire](../README.md)

