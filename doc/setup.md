# Utilisation



## 1. Faire marcher l'application sur Ipad

Deux possibiltés distinctes:  

### a. Utiliser Expo (environnement de développement)

Une fois React-native et Expo d'installés, il suffira de faire lancer le serveur de développement local.

Voir la partie [Installation](./installation.md) de la documentation pour plus d'informations.



### b. Utiliser un build `.ipa` et l'installer sur la tablette

Nécessite un compte Apple Développeur.



## 2. Lancer le serveur Docker

Télécharger (ce repo github)(https://github.com/GautierArcin/dockerAppli/tree/master/docker_flask_api_5mn).

Se placer dans le dossier `Docker` (contenant le fichier `docker-compose.yml`).

L'image Docker va commencer à être build. Une fois qu'elle sera build, le serveur démarrera automatiquement.

***Note:*** *Bien vérifier les dépendances. L'algo a besoin de nvidia-docker et d'une carte graphique nvidia.*



## 3. Vérifier la communication entre application et backend

L'application est conçu pour communiquer avec une backend locale. Comme les données médicales sont des données très sensibles, le serveur peut, si on le désire, avoir juste un accès sur le réseau local et non-internet.


### a. Vérifier que l'ipad et le serveur sont sur le même réseau


### b. Entrer la commande `ifconfig` dans un terminal

Elle devrait afficher une adresse ip locale, comme ceci :

![exemple ip locale ifconfig](./ressources/utilisation/1.png)


### c. Rentrer l'adresse ip (avec port) dans l'application

Une option est disponible pour rentrer l'ip avec port dans la fenêtre locale de l'application :

![exemple ip locale application](./ressources/utilisation/2.png)


**Note:** L'ip va être de la forme `http://[adresse-ip-locale]:[port serveur docker]/` avec `5000` comme port par défaut.
*Exemple: `http://192.168.1.2:5000/`*

---

### [Retour sommaire](../README.md)

