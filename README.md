![Logo](./doc/ressources/intel.png)

MVP d'une application pour la mesure de l'intelligibilité dans le cadre de la parole pathologique , dans le but de permettre aux ORL de Toulouse de disposer d'un outil de plus pour suivre leurs patients. [Vidéo de démonstration](http://www.youtube.com/watch?v=9iW20suHMR4).

![test](./doc/ressources/presentation/enregistrement.gif)

# Sommaire

## - [Fonctionnalités](./doc/fonctionnalites.md)

## - [Setup initial](./doc/setup.md)

## - [Guide d'utilisation ORL](./doc/guide.md)

## - Développement

## &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- [Installation](./doc/installation.md)

## &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- [Architecture Projet](./doc/archi.md)

## &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- [Débogage](./doc/debogage.md)

## &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- [Tests](./doc/test.md)

## - [Ajout d'un algorithme sur l'application](./doc/route.md)

## [- Lien divers](./doc/Ressources.md)

<br>
